#!/usr/bin/env python3.12
# -*- coding: utf-8 -*-

"""
    test.py - Simple Python command line application.

    Gets information regarding followers and following counts for my
    main Mastodon account.

    Copyright (C) 2021  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 3 of the GNU General Public License
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

from mastodon import Mastodon, MastodonError
from argparse import ArgumentParser
from collections import Counter
from pathlib import Path

import os
import logging
import stackprinter

stackprinter.set_excepthook(style="color")

DEFAULT_API_URL = "cyberpunk.lol"
DEFAULT_TOKEN_PATH = "~/.tokens/"
DEFAULT_TOKEN = "cyberpunk_acctoken.secret"


def do_matches(f_list: list, which: str):
    """Helper function to output user info."""

    logging.debug(args.instancename)

    for iname in args.instancename:
        matches = list(filter(lambda x: (iname.casefold() in x), sorted(f_list)))

        if len(matches) > 0:

            match(which):
                case "followers":
                    print("Followers\n=========\n")

                case "following":
                    print("Following\n=========\n")

                case "mutuals":
                    print("Mutuals\n=======\n")

            for instance in matches:
                print(instance)

            print()

    print()


def do_print(name: str, info: dict) -> None:
    """Function to cut down on code duplication."""

    output = f"Instance:  {name}\nFollowers: {info[instance][0]}\n"
    output += f"Following: {info[instance][1]}\n"

    print(output)


if __name__ == "__main__":
    try:
        parser = ArgumentParser()

        parser.description = "Display number of followers and following."

        parser.add_argument("instancename", nargs="*", type=str, help="instance_name")

        parser.add_argument(
            "-o", "--origin", type=str, nargs="?", default=None, help="home instance"
        )

        parser.add_argument(
            "-t",
            "--token",
            type=str,
            nargs="?",
            default=None,
            help="home instance access token file",
        )

        parser.add_argument(
            "-a", "--all", help="show all instances' counts", action="store_true"
        )

        parser.add_argument(
            "-d", "--debug", help="turn debugging on", action="store_true"
        )

        parser.add_argument(
            "-u",
            "--user",
            help="show username of follower, in a special case",
            action="store_true",
        )

        args = parser.parse_args()

        # If we've forgotten the optional -a / --all and an instance
        #  name then default to printing all instances' follows
        #  and following info.
        if args.instancename == [] and args.all is False:
            args.all = True

        if args.debug:
            log_level = logging.DEBUG

        else:
            log_level = logging.INFO

        logging.basicConfig(level=log_level, format="%(levelname)s - %(message)s")

        # The client's access token needs 'read' API access to the Mastodon
        #  account that you want to get the followers/following lists from.

        if args.origin is not None:
            api_url = f"https://{args.origin}/"

        else:
            api_url = f"https://{DEFAULT_API_URL}/"

        if args.token is not None and Path(args.token).exists():
            token = args.token

        else:
            token_loc = f"{DEFAULT_TOKEN_PATH}{DEFAULT_TOKEN}"
            token = os.path.expanduser(f"{token_loc}")

        mastodon = Mastodon(
            access_token=token,
            api_base_url=api_url,
        )

        logging.debug(f"{mastodon=}")

        me = mastodon.me()

        logging.debug(f"{me=}")

        followers_pages = []
        following_pages = []

        # Since the followers and following lists are paginated, with a max
        #  of 40 accounts per 'page', we get the first pages, for each.
        followers_pages.append(mastodon.account_followers(id=me.id))
        following_pages.append(mastodon.account_following(id=me.id))

        # Now, you just need to call fetch_remaining() with the first
        #  page's info from each of the previous calls
        #  to get a list of _all_ the followers/following in one go.
        all_followers = mastodon.fetch_remaining(followers_pages[0])
        all_following = mastodon.fetch_remaining(following_pages[0])

        followers_instances = []
        following_instances = []

        for follower in all_followers:
            # We don't need the front
            url = follower.url.removeprefix("https://")

            # Do we actually want to list the followers usernames?
            if args.user and not args.all:
                followers_instances.append(url)
            else:
                # No, we don't need the username,
                # so split it off the url.
                followers_instances.append(url.split("/")[0])

        for following in all_following:
            # Again, just isolate and use the actual instance name
            url = following.url.removeprefix("https://")

            # Again, do we want to list the following usernames?
            if args.user and not args.all:
                following_instances.append(url)
            else:
                # Again, split it off if we don't need it.
                following_instances.append(url.split("/")[0])

        if args.user and not args.all:
            do_matches(followers_instances, "followers")

            do_matches(following_instances, "following")

            mutuals = list(set(followers_instances) & set(following_instances))

            do_matches(mutuals, "mutuals")

            exit(0)

        # IF we are just counting, and not naming users, carry on...

        # Use Counter classes since they'll do the counting
        #  for us.
        follower_instances_counter = Counter(followers_instances)
        following_instances_counter = Counter(following_instances)

        # Now to combine the Counters into a single dictionary
        #  where each instance name from followers_instances is
        #  used as the key, with a tuple of the no. of followers
        #  and following stored as the value.

        combined_dict = {}

        for instance in followers_instances:
            combined_dict[instance] = (
                follower_instances_counter[instance],
                following_instances_counter[instance],
            )

        # Do the same for instances in following_instances, and
        #  again store in the combined_dict, as above.

        for instance in following_instances:
            combined_dict[instance] = (
                follower_instances_counter[instance],
                following_instances_counter[instance],
            )

        # If we want to list all instances' followers and following
        if args.all is True:
            # Loop through the sorted combined_dictionary's keys
            #  and output the information
            for instance in sorted(combined_dict.keys()):
                # Print the information out
                do_print(instance, combined_dict)

        # Otherwise just list the followers and following for the
        #  requested instances.
        else:
            # Loop through each instance given by the
            #  user on the command-line.
            for iname in args.instancename:
                # Iterate through the sorted combined_dictionary's keys
                #  and only output information for each matching
                #  instance that has been requested by the
                #  user.

                matches = list(
                    filter(
                        lambda x: (iname.casefold() in x), sorted(combined_dict.keys())
                    )
                )

                for instance in matches:
                    do_print(instance, combined_dict)

    except MastodonError as err:
        logging.error(err)
