#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
    test.py - Simple Python command line application.

    Gets information regarding followers and following counts for my
    main Mastodon account.

    Copyright (C) 2021  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 3 of the GNU General Public License
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

from mastodon import Mastodon, MastodonError
from argparse import ArgumentParser
from collections import Counter

import logging
import stackprinter

stackprinter.set_excepthook(style="color")


def do_print(name: str, info: dict) -> None:
    """Function to cut down on code duplication."""

    output = f"Instance:  {name}\nFollowers: {info[instance][0]}\n"
    output += f"Following: {info[instance][1]}\n"

    print(output)


if __name__ == "__main__":

    try:

        parser = ArgumentParser()

        parser.description = "Display number of followers and following."

        parser.add_argument("instancename", nargs="*", type=str, help="instance_name")

        parser.add_argument(
            "-a", "--all", help="show all instances' counts", action="store_true"
        )

        parser.add_argument(
            "-d", "--debug", help="turn debugging on", action="store_true"
        )

        args = parser.parse_args()

        # If we've forgotten the optional -a / --all and an instance
        #  name then default to printing all instances' follows
        #  and following info.
        if args.instancename == [] and args.all is False:

            args.all = True

        if args.debug:

            log_level = logging.DEBUG

        else:

            log_level = logging.INFO

        logging.basicConfig(level=log_level, format="%(levelname)s - %(message)s")

        # The client's access token needs 'read' API access to the Mastodon
        #  account that you want to get the followers/following lists from.
        mastodon = Mastodon(
            access_token="/home/qyv/Documents/Important/dummy/dummy_usercred.secret",
            api_base_url="https://mastodon.xyz/",
        )

        logging.debug(f"{mastodon=}")

        me = mastodon.me()

        logging.debug(f"{me=}")

        followers_pages = []
        following_pages = []

        # Since the followers and following lists are paginated, with a max
        #  of 40 accounts per 'page', we get the first pages, for each.
        followers_pages.append(mastodon.account_followers(id=me.id))
        following_pages.append(mastodon.account_following(id=me.id))

        # Now, you just need to call fetch_remaining() with the first
        #  page's info from each of the previous calls
        #  to get a list of _all_ the followers/following in one go.
        all_followers = mastodon.fetch_remaining(followers_pages[0])
        all_following = mastodon.fetch_remaining(following_pages[0])

        followers_instances = []
        following_instances = []

        for follower in all_followers:

            # We don't need the front
            url = follower.url.removeprefix("https://")

            # We also don't need the username
            followers_instances.append(url.split("/")[0])

        for following in all_following:

            # Again, just isolate and use the actual instance name
            url = following.url.removeprefix("https://")
            following_instances.append(url.split("/")[0])

        # Use Counter classes since they'll do the counting
        #  for us.
        follower_instances_counter = Counter(followers_instances)
        following_instances_counter = Counter(following_instances)

        # Now to combine the Counters into a single dictionary
        #  where each instance name from followers_instances is
        #  used as the key, with a tuple of the no. of followers
        #  and following stored as the value.

        combined_dict = {}

        for instance in followers_instances:

            combined_dict[instance] = (
                follower_instances_counter[instance],
                following_instances_counter[instance],
            )

        # Do the same for instances in following_instances, and
        #  again store in the combined_dict, as above.

        for instance in following_instances:

            combined_dict[instance] = (
                follower_instances_counter[instance],
                following_instances_counter[instance],
            )

        # If we want to list all instances' followers and following
        if args.all is True:

            # Loop through the sorted combined_dictionary's keys
            #  and output the information
            for instance in sorted(combined_dict.keys()):

                # Print the information out
                do_print(instance, combined_dict)

        # Otherwise just list the followers and following for the
        #  requested instances.
        else:

            # Loop through each instance given by the
            #  user on the command-line.
            for iname in args.instancename:

                # Iterate through the sorted combined_dictionary's keys
                #  and only output information for each matching
                #  instance that has been requested by the
                #  user.

                matches = list(
                    filter(
                        lambda x: (iname.casefold() in x), sorted(combined_dict.keys())
                    )
                )

                for instance in matches:

                    do_print(instance, combined_dict)

    except MastodonError as err:

        logging.error(err)
