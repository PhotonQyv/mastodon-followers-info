# mastodon-followers-info

Small Python script that gets the Followers/Following list from a Mastodon account, 
and displays them. User can enter instance names on the command-line, and have the 
followers/following counts from those instances only, or optionally list all 
followers/following counts from all instances that the user follows/is following.
